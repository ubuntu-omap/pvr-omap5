#! /bin/bash

INSTALL_LIST="\
libegl1-sgx-omap5.install \
libgles1-sgx-omap5.install \
libgles2-sgx-omap5.install \
libopenvg1-sgx-omap5.install \
pvr-omap5.install \
pvr-omap5-utils.install"

for f in $INSTALL_LIST
do 
  debugf=`echo $f | awk -F. '{print $1"-dbg."$2}'`
  echo $debugf
  cp $f $debugf
  sed -i 's/^usr/usr\/lib\/debug\/usr/g' $debugf
done


